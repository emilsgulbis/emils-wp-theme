<?php get_header(); ?>
	
	<?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'emils' ); ?>

	<?php get_search_form(); ?>
	
	<?php the_widget( 'WP_Widget_Recent_Posts' ); ?>
	
<?php get_footer(); ?>
