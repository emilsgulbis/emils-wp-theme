<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<div class="input">
		<input type="search" class="form-control" placeholder="<?php _e('Looking for something?', 'goaff'); ?>" value="<?php echo get_search_query() ?>" name="s" autocomplete="off">
	</div>
	<button type="submit" class="btn btn-icon">
		<i class="df df-search"></i>
	</button>
</form>