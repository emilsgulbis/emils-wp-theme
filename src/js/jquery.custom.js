$(function(){
	// Google maps
	if($("#map-canvas").length == 1){
	    var $map = $('#map-canvas');
	    var center = {lat: parseFloat($map.data('lat')), lng: parseFloat($map.data('lng'))};

	    var map = new google.maps.Map($map[0], {
	        zoom: parseInt($map.data('zoom')),
	        center: center,
	        disableDefaultUI: true,
	        scrollwheel: false,
	        zoomControl: true,
	        styles : [{"stylers":[{"hue":"#2991d6"},{"saturation":0}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":30},{"visibility":"simplified"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]}]
	    });

	    var marker = new google.maps.Marker({
	        position: center,
	        map: map,
	        icon: $map.data('marker')
	    });
	}

	$('.hamburger').on('click', function(){
		console.log('test')
	    $('.mobile-navbar').toggleClass('hidden');
	});
});