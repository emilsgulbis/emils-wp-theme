<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta name="theme-color" content="#c49a6c">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<title><?php wp_title(); ?></title>
<?php get_template_part('template-parts/header', 'favicon'); ?>
<?php wp_head(); ?>
</head>
<body>

<header id="header">
	<?php get_template_part('template-parts/header', 'logo'); ?>
	<?php get_template_part('template-parts/header', 'menu'); ?>
</header>

<div id="content">
