<?php

add_action( 'tf_create_options', 'emils_create_options' );

function emils_create_options() {

    $titan = TitanFramework::getInstance( 'emils' );

    $panel = $titan->createAdminPanel( array(
        'name' => __('Theme options', 'emils'),
        'position' => 29
    ) );

    $panel->createOption( array(
        'type' => 'save'
    ) );

    // Style
    $styleTab = $panel->createTab(array(
        'name' => __('Style', 'emils')
    ));

    $styleTab->createOption( array(
        'name'  => __('Favicon', 'emils'),
        'id'    => 'favicon',
        'type'  => 'upload'
    ));  

    $styleTab->createOption( array(
        'name'  => __('Logo 2x', 'emils'),
        'id'    => 'logo_2x',
        'type'  => 'enable',
        'default' => true
    ));  

    $styleTab->createOption( array(
        'name'  => __('Logo', 'emils'),
        'id'    => 'logo',
        'type'  => 'upload'
    ));  
    $styleTab->createOption( array(
        'name'  => __('Footer copyright text', 'emils'),
        'id'    => 'copyright',
        'type'  => 'text'
    ));  

    // Contact
    $contactTab = $panel->createTab(array(
        'name' => __('Contact information', 'emils')
    ));
    
    $contactTab->createOption( array(
        'name'  => __('Phone number', 'emils'),
        'id'    => 'phone',
        'type'  => 'text'
    )); 

    $contactTab->createOption( array(
        'name'  => __('Email address', 'emils'),
        'id'    => 'email',
        'type'  => 'text'
    )); 
}
