<?php

function load_shortcode( $path = '', $data = array() ) {

	$full_path = EMILS_THEME_DIR . '/shortcodes/' . $path . '.php';
	
	if ( file_exists( $full_path ) ) {
		require $full_path;
	} else {
		throw new Exception( 'The shortcode path ' . $full_path . ' can not be found.' );
	}
}

function vc_enqueue() {
	wp_enqueue_style('backend-vc-css', get_template_directory_uri() . '/admin/visual-composer/assets/vcViews.css');
    wp_enqueue_script( 'backend-vc', get_template_directory_uri() . '/admin/visual-composer/assets/vcViews.js', array( 'vc-backend-min-js' ), '1.0', true);
}

add_action( 'vc_backend_editor_render', 'vc_enqueue');

foreach ( glob( plugin_dir_path( __FILE__ ) . "visual-composer/*.php" ) as $file ) {
    include_once $file;
}