<?php

if(function_exists(('vc_map'))){
	add_action( 'vc_before_init', 'add_remove_vc_row_inner_params' );
}

function add_remove_vc_row_inner_params(){
	$remove_row_params = array(
		'gap',

		// 'full_width',	
		// // 'full_height',
		'equal_height',
		// // 'columns_placement',
		'content_placement',

		// 'video_bg',

		// 'parallax',
		// 'video_bg_url',
		// 'parallax_speed_video',
		// 'parallax_speed_bg',
		// 'video_bg_parallax',
		// 'parallax_image',

		'el_id',
		'el_class',
		'css'
	);
	foreach($remove_row_params as $param){
		vc_remove_param('vc_row_inner', $param);
	}
}