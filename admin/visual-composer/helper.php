<?php

if(!function_exists('tag_array')){
	function tag_array(){
		return array(
			__('Default (div)', 'emils') => '',
			'H1' => 'h1',
			'H2' => 'h2',
			'H3' => 'h3',
			'H4' => 'h4',
			'H5' => 'h5',
			'H6' => 'h6',
			'p' => 'p',
			'span' => 'span',
			'div' => 'div'
		);
	}
}

if(!function_exists('align_array')){
	function align_array(){
		return array(
			__('Default (left)', 'emils') => '',
			__('Center', 'emils') => 'center',
			__('Right', 'emils') => 'right',
			__('Justify', 'emils') => 'justify',
		);
	}
}

if(!function_exists('size_array')){
	function size_array(){
		return array(
			__('Default', 'emils') => '',
			__('Large', 'emils') => 'lg',
			__('Extra large', 'emils') => 'xl',
			__('Small', 'emils') => 'sm',
		);
	}
}

if(!function_exists('style_array')){
	function style_array(){
		return array(
			__('Default (light)', 'emils') => 'default',
			__('Primary (red)', 'emils') => 'primary',
			__('Success (green)', 'emils') => 'success',
		);
	}
}