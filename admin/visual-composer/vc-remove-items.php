<?php

if(function_exists(('vc_map'))){
	add_action( 'vc_before_init', 'add_remove_vc_items' );
}

function add_remove_vc_items(){
	$remove_items = array(
		'vc_column_text',
		'vc_icon',
		'vc_separator',
		'vc_text_separator',
		'vc_message',
		'vc_facebook',
		'vc_tweetmeme',
		'vc_googleplus',
		'vc_pinterest',
		'vc_toggle',
		'vc_single_image',
		'vc_gallery',
		'vc_images_carousel',
		'vc_tta_tabs',
		'vc_tta_tour',
		'vc_tta_accordion',
		'vc_tta_section',
		'vc_tta_pageable',
		'vc_cutom_heading',
		'vc_btn',
		'vc_cta',
		'vc_custom_heading',
		'vc_widget_sidebar',
		'vc_posts_slider',
		'vc_video',
		'vc_gmaps',
		'vc_raw_html',
		'vc_raw_js',
		'vc_flickr',
		'vc_progress_bar',
		'vc_pie',
		'vc_round_chart',
		'vc_line_chart',
		'vc_basic_grid',
		'vc_media_grid',
		'vc_masonry_grid',
		'vc_masonry_media_grid',
		'vc_tabs',
		'vc_tour',
		'vc_button',
		'vc_button2',
		'vc_cta_button',
		'vc_cta_button2',
		'vc_accordion',

		'vc_wp_search',
		'vc_wp_meta',
		'vc_wp_recentcomments',
		'vc_wp_calendar',
		'vc_wp_pages',
		'vc_wp_tagcloud',
		'vc_wp_custommenu',
		'vc_wp_text',
		'vc_wp_posts',
		'vc_wp_categories',
		'vc_wp_archives',
		'vc_wp_rss',
	);
	foreach($remove_items as $item){
		vc_remove_element($item);
	}
}