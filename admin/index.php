<?php

// Post types
foreach ( glob( plugin_dir_path( __FILE__ ) . "post-types/*.php" ) as $file ) {
    include_once $file;
}

// Theme settings
require_once( 'titan-framework/titan-framework-embedder.php' );
require_once( 'titan-options.php' );

// Visual Composer
require_once('vc.php');