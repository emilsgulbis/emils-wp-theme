<?php
	$titan = TitanFramework::getInstance( 'emils' );
	$favicon = wp_get_attachment_url($titan->getOption('favicon'));
	if(!$favicon)
		return;
?>
<link rel="shortcut icon" href="<?php echo $favicon; ?>" type="image/x-icon">