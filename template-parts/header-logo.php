<?php
	$titan = TitanFramework::getInstance( 'emils' );
	
	$logo = wp_get_attachment_image_src($titan->getOption('logo'), 'full');

	if(!$logo)
		return;

	$logo2x = $titan->getOption('logo_2x');
	if($logo2x){
		$logo[1] = $logo[1]/2;
		$logo[2] = $logo[2]/2;
	}
?>

<div id="logo">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php _e('To home page', 'emils'); ?>">
		<img src="<?php echo $logo[0] ;?>" width="<?php echo $logo[1]; ?>" height="<?php echo $logo[2]; ?>" alt="<?php echo get_bloginfo('name'); ?>">
	</a>
</div>