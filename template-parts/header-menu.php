<?php
	$menu = array(
		'theme_location' => 'primary', 
		'container_class' => 'navbar',
		'menu_class'=>'navbar-nav nav',
		'link_before' => '<span>',
		'link_after' => '</span>',
	); 
?>
<div id="menu">
	<div class="hidden-sm hidden-xs">
		<?php wp_nav_menu( $menu ); ?>
	</div>
	<div class="visible-sm visible-xs">
		<?php 
			$menu['container_class'] .= ' mobile-navbar hidden'; 
			wp_nav_menu( $menu );
		?>
		<div class="hamburger"><span></span></div>
	</div>
</div>