<?php

define( 'EMILS_THEME_DIR', get_template_directory() );
define( 'EMILS_ADMIN_DIR', EMILS_THEME_DIR . '/admin' );

if ( ! function_exists( 'emils_setup' ) ) :

function emils_setup() {

	load_theme_textdomain( 'emils', get_template_directory() . '/languages' );

	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'post-thumbnails' );
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary Menu', 'emils' ),
	) );

	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

}
endif; 

add_action( 'after_setup_theme', 'emils_setup' );

function emils_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'emils' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<div class="widget-title"><h4>',
		'after_title'   => '</h4></div>',
	) );
}
add_action( 'widgets_init', 'emils_widgets_init' );

function emils_scripts() {
	wp_enqueue_style( 'emils-style', get_template_directory_uri() . '/css/all.css');

	wp_register_script('emils-script', get_template_directory_uri() . '/js/all.min.js');
	wp_localize_script( 'emils-script', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	wp_enqueue_script( 'emils-script');

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'emils_scripts' );

add_filter( 'show_admin_bar', '__return_false' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Widgets
foreach ( glob( plugin_dir_path( __FILE__ ) . "widgets/*.php" ) as $file ) {
    include_once $file;
}

require_once('admin/index.php');

// Image sizes
// add_image_size( 'thumbnail_wide', 350, 200, true );